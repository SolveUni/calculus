#
#  Makefile
#
#  Copyright 2016 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

ifndef V
V := 0
endif

ifeq ($(V),0)
quiet_q   := -q
todevnull := >/dev/null
endif

INSTALL  ?= install
LATEX    ?= latex
LN_S     ?= ln -s
PYTHON   ?= python3

LATEXARGS ?= -halt-on-error

COURSE   ?= Calculus
URL      ?= https://gitlab.com/lilrc/calculus

# Where to install pdf files. It is set to this very strange default
# since it is what I (Karl Lindén) use. Please do change this when
# something better than someones private ownCloud is used.
DESTDIR  ?= /home/kalle/ownCloud/calculus/

# License, very important.
license := COPYING-FDL-1.3

# Beware! Do not put any manually created files here!
texfiles := \
	answers.tex \
	exercises.tex \
	full.tex \
	solutions.tex
dvifiles := $(texfiles:.tex=.dvi)
pdffiles := $(texfiles:.tex=.pdf)
psfiles  := $(texfiles:.tex=.ps)

# All files that should be installed
installfiles := $(pdffiles) $(license)

find = $(shell find -mindepth 1 -maxdepth 1 -type $(1) -name '$(2)' -execdir basename -a {} +)

subdirs   := $(call find,d,*-chap)
licenses  := $(addsuffix /$(license), $(subdirs))
makefiles := $(addsuffix /Makefile, $(subdirs))
tools     := $(addsuffix /tools, $(subdirs))
links     := $(licenses) $(makefiles) $(tools)

subdirarg     := --subdirs="$(subdirs)"
genlatex_args := --course="$(COURSE)" --url="$(URL)"
genauthors    := $(PYTHON) tools/genauthors.py $(subdirarg)
genlatex      := $(PYTHON) tools/genlatex.py $(subdirarg) $(genlatex_args)

answers   := $(call find,f,*-ans*)
exercises := $(call find,f,*-ex*)
solutions := $(call find,f,*-sol*)

cache     := authors.cache

cleanfiles := $(cache)
cleanfiles += $(dvifiles)
cleanfiles += $(psfiles)
cleanfiles += $(pdffiles)
cleanfiles += $(texfiles)
cleanfiles += $(texfiles:.tex=-list.tex)
cleanfiles += $(texfiles:.tex=.aux)
cleanfiles += $(texfiles:.tex=.log)
cleanfiles += $(texfiles:.tex=.out)

distcleanfiles := $(links)

.DELETE_ON_ERROR:

allsubdirs := $(addprefix all-,$(subdirs))
all: $(allsubdirs) $(cache) $(pdffiles)
$(allsubdirs): all-%: % links
	$(MAKE) -C $<
.PHONY: all $(allsubdirs)

cachesubdirs := $(addprefix cache-,$(subdirs))
cache: $(cache) $(cachesubdirs)
$(cachesubdirs): cache-%: % links
	$(MAKE) -C $< cache
.PHONY: cache $(cachesubdirs)

texsubdirs := $(addprefix tex-,$(subdirs))
tex: $(texsubdirs) $(cache) $(texfiles)
$(texsubdirs): tex-%: % links
	$(MAKE) -C $< tex
.PHONY: tex $(texsubdirs)

dvisubdirs := $(addprefix dvi-,$(subdirs))
dvi: $(dvisubdirs) $(cache) $(dvifiles)
$(dvisubdirs): dvi-%: % links
	$(MAKE) -C $< dvi
.PHONY: dvi $(dvisubdirs)

pdfsubdirs := $(addprefix pdf-,$(subdirs))
pdf: $(pdfsubdirs) $(cache) $(pdffiles)
$(pdfsubdirs): pdf-%: % links
	$(MAKE) -C $< pdf
.PHONY: pdf $(pdfsubdirs)

pssubdirs := $(addprefix ps-,$(subdirs))
ps: $(pssubdirs) $(cache) $(psfiles)
$(pssubdirs): ps-%: % links
	$(MAKE) -C $< ps
.PHONY: ps $(pssubdirs)

installsubdirs := $(addprefix install-,$(subdirs))
installrules := $(addprefix install-,$(installfiles))
install: all $(installsubdirs) $(installrules)
$(installrules): install-%: % destdir
	$(INSTALL) --mode=644 $< $(DESTDIR)
destdir:
	$(INSTALL) -d $(DESTDIR)
$(installsubdirs): install-%: % all-% links
	$(MAKE) -C $< install DESTDIR=$(DESTDIR)/$<
.PHONY: install destdir $(installrules) $(installsubdirs)

links: $(links)
$(links):
	$(LN_S) ../$(notdir $@) $@

cleansubdirs := $(addprefix clean-,$(subdirs))
clean: $(cleansubdirs)
	-rm -f $(cleanfiles)
$(cleansubdirs): clean-%: % $(makefiles)
	$(MAKE) -C $< clean
.PHONY: clean $(cleansubdirs)

distcleansubdirs := $(addprefix distclean-,$(subdirs))
distclean: clean $(distcleansubdirs)
	-rm -f $(distcleanfiles)
$(distcleansubdirs): distclean-%: % $(makefiles)
	$(MAKE) -C $< distclean
.PHONY: distclean $(distcleansubdirs)

subdiranswers   := $(addsuffix /answers-list.tex,$(subdirs))
subdirfull      := $(addsuffix /full-list.tex,$(subdirs))
subdirexercises := $(addsuffix /exercises-list.tex,$(subdirs))
subdirsolutions := $(addsuffix /solutions-list.tex,$(subdirs))
subdirtargets := \
	$(subdiranswers) \
	$(subdirfull) \
	$(subdirexercises) \
	$(subdirsolutions) \
	$(addsuffix /$(cache),$(subdirs))
$(subdirtargets): $(texsubdirs) $(cachesubdirs)

$(cache): $(subdirtargets) $(answers) $(exercises) $(solutions)
	$(genauthors) --authors=$@ $(answers) $(exercises) $(solutions)

answers-list.tex: $(subdiranswers) $(answers)
	$(genlatex) \
		--answers \
		--list=answers-list.tex \
		--output=answers.tex \
		$(answers)

exercises-list.tex: $(subdirexercises) $(exercises)
	$(genlatex) \
		--exercises \
		--list=exercises-list.tex \
		--output=exercises.tex \
		$(exercises)

full-list.tex: $(subdirfull) $(answers) $(exercises) $(solutions)
	$(genlatex) \
		--answers \
		--exercises \
		--solutions \
		--list=full-list.tex \
		--output=full.tex \
		$(answers) $(exercises) $(solutions)

solutions-list.tex: $(subdirsolutions) $(solutions)
	$(genlatex) \
		--solutions \
		--list=solutions-list.tex \
		--output=solutions.tex \
		$(solutions)

$(texfiles): %.tex : %-list.tex

# This rule tries to only depend on the exact LaTeX sources to avoid
# unnecessary rebuilds.
%.dvi %.aux %.log %.out: %.tex %-list.tex $(addsuffix /%-list.tex,$(subdirs))
	$(LATEX) $(LATEXARGS) $< $(todevnull)
	while grep -q 'Rerun to get ' $*.log ; do $(LATEX) $(LATEXARGS) $< $(todevnull) ; done

%.ps: %.dvi
	dvips $(quiet_q) $< -o $@

%.pdf: %.ps
	ps2pdf $(quiet_q) $< $@
